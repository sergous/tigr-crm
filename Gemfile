source 'https://rubygems.org'

gem 'rails', '~> 4.1'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'coffee-rails'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  #gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'

  gem 'bootstrap-sass', '~> 3.3.4'
  gem 'sass-rails', '>= 3.2'
  gem 'font-awesome-rails', '4.3.0.0'
  gem 'will_paginate-bootstrap'
end

gem 'jquery-rails'
gem 'jquery-ui-rails'

group :development, :test do
  gem 'sqlite3'
  gem 'rspec-rails'
end

group :development do
  gem 'guard-rspec'
  gem 'quiet_assets'
  gem 'letter_opener'
end

group :test do
  gem 'capybara'
  gem 'rb-inotify'
  gem 'libnotify'
  gem 'guard-spork'
  gem 'spork'
  gem 'factory_girl_rails'
end

group :production do
  gem 'pg'
end

gem 'slim-rails'
gem 'devise', '~> 3.2'
gem 'devise_token_auth'
gem 'cancan'
gem 'simple_form'
gem 'kaminari-bootstrap', '~> 0.1.3'
gem 'faker'
gem 'rmagick'
gem 'carrierwave'
gem 'country_select'
gem 'nilify_blanks'
gem 'omniauth'
gem 'omniauth-twitter'
gem 'omniauth-vkontakte'
gem 'omniauth-facebook'
gem 'ckeditor'
gem 'rubyzip'
gem 'protected_attributes'

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
gem 'unicorn'

gem 'puma'
# Deploy with Capistrano
gem 'capistrano'

gem 'annotate', '>=2.5.0'
gem 'russian'
gem 'whenever', require: false

gem 'newrelic_rpm'
gem 'sanitize'
gem 'turbolinks'