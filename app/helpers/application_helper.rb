module ApplicationHelper
  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    link_to(image_tag("add_small.png") + name, '#', class: "add_#{association.to_s.singularize}_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end

  def action_type_list
    ActionType.all.map { |type| [t(".#{type.name}"), type.id] }
  end

  def email_type_list
    EmailType.all.map { |type| [t("types.email.#{type.name}"), type.id] }
  end

  def phone_type_list
    PhoneType.all.map { |type| [t("types.phone.#{type.name}"), type.id] }
  end

  def is_active_controller(controller_name)
      params[:controller] == controller_name ? "active" : nil
  end

  def is_active_action(action_name)
      params[:action] == action_name ? "active" : nil
  end
end