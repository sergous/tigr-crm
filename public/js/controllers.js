/**
 * Main controller.js file
 *
 * Functions (controllers)
 *  - ContactsCtrl
 *  - DealsCtrl
 *
 */

var host = 'http://0.0.0.0:3000';
var getRandom = function(max) {
  return Math.floor((Math.random()*max)+1);
}

/**
 * ContactsCtrl - controller
 * Contains contacts data used.
 *
 */
function ContactsCtrl($scope, $http) {

  $scope.persons = [];

  // GET Persons:
  $http.get(host + '/ru/people.json').
      success(function (data) {
          $scope.persons = data;
          var arrayLength = data.length;
          for(var i=0; i<arrayLength; i++) {
            $scope.persons[i].photo_id = getRandom(6);
          }
      });

  $scope.companies = [];

  // GET Contacts:
  $http.get(host + '/ru/contacts.json').
      success(function (data) {
          $scope.companies = data;
      });
}

/**
 * DealsCtrl - controller
 * Contains deals data used.
 *
 */
function DealsCtrl($scope, $http) {

  $scope.deals = [];

  // GET Deals:
  $http.get(host + '/ru/deals.json').
      success(function (data) {
          $scope.deals = data;
      });
}

function TasksCtrl($scope, $http) {

  $scope.tasks = [];

  // GET Tasks:
  $http.get(host + '/ru/tasks.json').
      success(function (data) {
          $scope.tasks = data;
      });
}

/**
 * Pass all functions into module
 *
 */
angular
    .module('inspinia')
    .controller('ContactsCtrl', ContactsCtrl)
    .controller('DealsCtrl', DealsCtrl)
    .controller('TasksCtrl', TasksCtrl)
