function htmlToPlaintext() {
  return function(text) {
    return String(text).replace(/<[^>]+>/gm, '');
  }
}

angular
    .module('inspinia')
    .filter('htmlToPlaintext', htmlToPlaintext)