/**
 * INSPINIA - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 */
function config($translateProvider) {

    $translateProvider
        .translations('en', {

            // Define all menu elements
            DASHBOARD: 'Dashboard',
            GRAPHS: 'Graphs',
            MAILBOX: 'Mailbox',
            WIDGETS: 'Widgets',
            FORMS: 'Forms',
            APPVIEWS: 'App views',
            OTHERPAGES: 'Other pages',
            UIELEMENTS: 'UI elements',
            MISCELLANEOUS: 'Miscellaneous',
            GRIDOPTIONS: 'Grid options',
            TABLES: 'Tables',
            GALLERY: 'Gallery',
            MENULEVELS: 'Menu levels',
            THIRDLEVEL: 'Third level',
            ANIMATIONS: 'Animations',
            LANDING: 'Landing page',
            LAYOUTS: 'Layouts',

            // Define some custom text
            WELCOME: 'Welcome Amelia',
            MESSAGEINFO: 'You have 42 messages and 6 notifications.',
            SEARCH: 'Search for something...',
            LANGUAGE: 'Language',
            LOGOUT: 'Log out',
            CONTACTS: 'Contacts',
            PERSONS: 'Persons',
            COMPANIES: 'Companies',
            DESIGN: 'Design',
            DEALS: 'Deals',
            SEARCHBUTTON: 'Search',
            DEALSLIST: 'All deals list',
            SUCCESSRATE: 'Success rate',
            CREATED: 'Created',
            TODO: 'Todo',

        })
        .translations('ru', {

            // Define all menu elements
            DASHBOARD: 'Сводка',
            GRAPHS: 'Графики',
            MAILBOX: 'Почта',
            WIDGETS: 'Виджеты',
            FORMS: 'Формы',
            APPVIEWS: 'Приложения',
            OTHERPAGES: 'Другое',
            UIELEMENTS: 'Интерфейс',
            MISCELLANEOUS: 'Разное',
            GRIDOPTIONS: 'Сетка',
            TABLES: 'Таблицы',
            GALLERY: 'Галерея',
            MENULEVELS: 'Подменю',
            THIRDLEVEL: 'Меню 3го уровня',
            ANIMATIONS: 'Анимация',
            LANDING: 'Лэндинг',
            LAYOUTS: 'Верстка',

            // Define some custom text
            WELCOME: 'Добро пожаловать!',
            MESSAGEINFO: 'У вас 42 новый сообщения и 6 уведомлений.',
            SEARCH: 'Введите поисковой запрос ...',
            LANGUAGE: 'Язык',
            LOGOUT: 'Выход',
            CONTACTS: 'Контакты',
            PERSONS: 'Люди',
            COMPANIES: 'Компании',
            DESIGN: 'Дизайн',
            DEALS: 'Сделки',
            SEARCHBUTTON: 'Найти',
            DEALSLIST: 'Список всех сделок',
            SUCCESSRATE: 'Прогноз успеха',
            CREATED: 'Создана',
            TODO: 'Задачи',
        });

    $translateProvider.preferredLanguage('ru');

}

angular
    .module('inspinia')
    .config(config)
